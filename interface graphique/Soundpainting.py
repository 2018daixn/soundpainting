from tkinter.filedialog import *
import pygame.mixer


import sys

sys.path.append('C:\\Users\\Enguerrand Monard\\soundpainting\\image vers son v2')
sys.path.append('C:\\Users\\Enguerrand Monard\\soundpainting\\Son_vers_image')

import master_ivs as mas
import fft_afficheur_temporel_v4 as fftat

#Chercher_son demande à l'utilisateur de sélectionner un son dans ses fichiers et renvoie le chemin
def Chercher_Son():
    chemin = askopenfilename(defaultextension = '.wav', title="Choisissez votre son en format wav")
    print(fftat.spectro(chemin, ''))

#Cocher_nbc demande à l'utilisateur de cocher noir et blanc ou couleur et de sélectionner une image dans ses fichiers et renvoit une liste [nbc, chemin]
def Chercher_Image():
    chemin = askopenfilename(defaultextension = '.png', title="Choisissez votre image en format png")
    print(mas.une_image_vers_son(chemin, 100, 5, 400, 'resultats_sons.wav'))
    pygame.mixer.init()
    pygame.mixer.music.load('resultats_sons/.wav')
    pygame.mixer.music.play()

###Mise en place de l'interface
Fenetre = Tk()

#La première ligne affiche le titre
titre = Label(Fenetre, text="Soundpainting")
titre.pack()

#La deuxième ligne affiche le bouton Son qui renvoit à "chercher_son"
button_son = Button(Fenetre, text='Son vers image', command=Chercher_Son)
button_son.pack()

#La troisième ligne affiche le bouton Image qui renvoit à "cocher_nbc"
button_image = Button(Fenetre, text='Image vers son', command=Chercher_Image)
button_image.pack()


Fenetre.mainloop()
