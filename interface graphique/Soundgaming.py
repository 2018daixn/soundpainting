from tkinter import *
import pygame.mixer
import random as rd

##Chemins d'accès des banques de son et d'images correspondantes
chemin_son = "../banque_so/" # mettre le chemin d'accès au son (ENGUERRAND)
chemin_images = "../banque_im/" # mettre le chemin d'accès aux images (ENGUERRAND)

#chemin_son = "../sounds/" # mettre le chemin d'accès au son (NATHAN)
#chemin_images = "../images/" # mettre le chemin d'accès aux images (NATHAN)

##Nombre d'images/sons dans la banque
nb_sons = 20 # (ENGUERRAND)
#nb_sons = 13 # (NATHAN)


##Dictionnaire des  {numero_image : nom_image}, dans tout le programme on a numero_imagek = numero_sonk et nom_imagek = nom_sonk

#Banque d'Enguerrand :
Banque={1:"Great_Wave_Wallpaper",2:"cs",3:"ddddh",4:"echiquier_spirale",5:"echiquier_spirale2",6:"fantome",7:"formes_bizarres",8:"image test paint 1",9:"image test paint 2",10:"interessant",11:"interessant2",12:"mindblow",13:"montgolfiere",14:"mosaiques",15:"radio",16:"random_square",17:"soleil",18:"spirale_couleur",19:"tissu_coton",20:"vagues"}
#Banque de Nathan :
#Banque={1:"1",2:"2",3:"3",4:"4",5:"5",6:"6",7:"7",8:"8",9:"9",10:"10",11:"11",12:"12",13:"13"}
#Banque = { i:"i" for i in range(1,14)}


def soundgaming():
    
    #Il s'agit de la première tentative 
    global premier_essai
    premier_essai=True

    #Quitter l'image
    def quitter() :
        try :
            pygame.mixer.music.stop()
        except :
            pass
        Fenetre.destroy()

    #Après que le joueur a appuyé sur le bouton "rejouer" ou "réessayer", on relance une nouvelle fenêtre
    def rejouer():
        Fenetre.destroy()
        soundgaming()
        
    #Après que le joueur a appuyé sur "jouer", on lance le jeu
    def jouer():
        button_jouer.destroy()

        
        ###Piocher 3 trois numéros au hasard qui correspondent à 3 images dans la banque d'image
        Ls = [i for i in range(1,nb_sons+1)]
    
        #Image1
        popped_1 = rd.randint(0,nb_sons - 1)
        numero_image1 = Ls[popped_1]
        Ls.pop(popped_1)
    
        #Image2
        popped_2 = rd.randint(0,nb_sons - 2)
        numero_image2 = Ls[popped_2]
        Ls.pop(popped_2)
    
        #Image3
        numero_image3 = Ls[rd.randint(0,nb_sons - 3)]


        ###Jouer le son correspondant à l'image 1

        nom_image1 = Banque[numero_image1]
        pygame.mixer.init()
        pygame.mixer.music.load(chemin_son + "speed=5 freq=100{0}.jpg.wav".format(nom_image1)) #Banque d'Enguerrand
        #pygame.mixer.music.load(chemin_son + "{0}.wav".format(nom_image1))  #Banque de Nathan
        pygame.mixer.music.play()


        ###Poser la question
        question = Label(Fenetre, text="Laquelle de ces images correspond au son que vous venez d'entendre ?")
        question.pack(side='top')


        ###Choisir au hasard l'ordre dans lequel les images seront affichées
        L=[numero_image1,numero_image2,numero_image3]

        #Première image (différent de image1 a priori)
        popped_1 = rd.randint(0,2)
        numero_premiere_image = L[popped_1]
        L.pop(popped_1)
    
        #Deuxième image
        popped_2 = rd.randint(0,1)
        numero_deuxieme_image = L[popped_2]
        L.pop(popped_2)
    
        #Troisième image
        numero_troisieme_image = L[0]


        ###Donner le nom correspondant aux numéros
    
        nom_premiere_image = Banque[numero_premiere_image]
        nom_deuxieme_image = Banque[numero_deuxieme_image]
        nom_troisieme_image = Banque[numero_troisieme_image]


        ###Donner une réponse (après que le jouer ait choisit une image)

        #Le joueur a donné la bonne réponse, un message de félicitations apparait et il peut rejouer en appuyant sur le bouton "rejouer"
        def bonne_reponse():
            global premier_essai
            premier_essai=False
            global reponse
            reponse = Label(Fenetre, text="Bonne réponse !!!")
            reponse.pack(side='top')
            global button_rejouer
            button_rejouer = Button(Fenetre, text="Rejouer", command=rejouer)
            button_rejouer.pack()
            pygame.mixer.music.stop()

        #Le joueur a donné la mauvaise réponse, "raté" apparait et il peut réessayer en appuyant sur le bouton "réessayer"
        def mauvaise_reponse():
            global premier_essai
            premier_essai=False
            if numero_premiere_image == numero_image1:
                bon_numero = 1
            elif numero_deuxieme_image == numero_image1:
                bon_numero = 2
            else :
                bon_numero = 3
            global reponse
            reponse = Label(Fenetre, text="Raté, la bonne réponse était l'image n°{0}".format(bon_numero))
            reponse.pack(side='top')
            global button_rejouer
            button_rejouer = Button(Fenetre, text="Réessayer", command=rejouer)
            button_rejouer.pack()
            pygame.mixer.music.stop()
            
        #Le joueur a appuyé sur la première image, on regarde si sa réponse est bonne ou non
        def reponseA():
            if premier_essai==False :
                global reponse
                reponse.destroy()
                global button_rejouer
                button_rejouer.destroy()
            if numero_premiere_image == numero_image1:
                bonne_reponse()
            else :
                mauvaise_reponse()

        #Le joueur a appuyé sur la deuxième image, on regarde si sa réponse est bonne ou non
        def reponseB():
            if premier_essai==False :
                global button_rejouer
                reponse.destroy()
                global button_rejouer
                button_rejouer.destroy()
            if numero_deuxieme_image == numero_image1:
                bonne_reponse()
            else :
                mauvaise_reponse()

        #Le joueur a appuyé sur la troisième image, on regarde si sa réponse est bonne ou non
        def reponseC():
            if premier_essai==False :
                global button_rejouer
                reponse.destroy()
                global button_rejouer
                button_rejouer.destroy()
            if numero_troisieme_image == numero_image1:
                bonne_reponse()
            else :
                mauvaise_reponse()
    
    
        ###Afficher les boutons et les images, chaque bouton renvoit à la fonction reponse qui lui correspond
        varGr = StringVar()
        varGr.set("B")
    
        #Premiere image pour le bouton A
        image_1 = PhotoImage(file=chemin_images + nom_premiere_image + ".png")
        bouton1 = Radiobutton(Fenetre, variable=varGr, value=numero_premiere_image, image=image_1, command=reponseA, text="1", compound="bottom",padx=10, width=333, height=400)
        bouton1.pack(side='left', expand=1)
    
        #Deuxième image pour le bouton B
        image_2 = PhotoImage(file=chemin_images + nom_deuxieme_image + ".png")
        bouton2 = Radiobutton(Fenetre, variable=varGr, value=numero_deuxieme_image, image=image_2, command=reponseB, text="2", compound="bottom",padx=10, width=333, height=400)
        bouton2.pack(side='left', expand=1)
    
        #Troisième image pour le bouton C
        image_3 = PhotoImage(file=chemin_images + nom_troisieme_image + ".png")
        bouton3 = Radiobutton(Fenetre, variable=varGr, value=numero_troisieme_image, image=image_3, command=reponseC, text="3", compound="bottom",padx=10, width=333, height=400)
        bouton3.pack(side='left', expand=1)

        Fenetre.mainloop()


    ###Mise en place de l'interface
    Fenetre = Tk()
    Fenetre.attributes('-fullscreen', 1)
    Button(Fenetre, text="Quit", command=quitter).pack()

    #La première ligne affiche le titre
    titre = Label(Fenetre, text="Soundgaming", padx=5, pady=15)
    titre.pack()

    #La deuxième ligne affiche le bouton Jouer qui renvoit à la fonction "jouer" définie plus haut
    button_jouer = Button(Fenetre, text="Jouer", command=jouer, padx =3,pady=5)
    button_jouer.pack()


    Fenetre.mainloop()

soundgaming()
