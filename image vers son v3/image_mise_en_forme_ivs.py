import numpy as np
from PIL import Image


def ouvre(chemin):
    '''

    :param chemin: string
    :return: objet image au sens de PIL
    '''
    return Image.open(chemin) # ouvre le fichier image


def pic_to_array_RGB(chemin, frequency):
    """
    :param chemin: string
    :param couleur: bool
    :param frequency: choose the output base frenquency in Hz
    :return: array des colonnes de l'image
    """
    base = ouvre(chemin)
    size = int(44100/frequency)
    # on redimensionne l'image pour la future FFT en convervant le rapport hauteur/largeur
    base = base.resize(( int(size*base.size[0]/base.size[1]) , size))
    # base.size = (largeur, hauteur)
    base2 = base.convert('RGB')
    imm = np.asarray(base2)
    # on convertit l'image en array
    return np.transpose(imm)


