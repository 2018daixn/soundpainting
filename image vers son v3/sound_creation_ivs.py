__name__ = 'sound_creation_ivs'

import numpy as np
import matplotlib.pyplot as plt
import scipy.io.wavfile as wave


def create_sound_list_RGB(A, speed):
    '''
    creates a stero out corresponding to the time view of the 3D matrix A with columns as frequencies and lines as time + real on R, imaginary on G, panning on B
    :param A: 3D array of RGB pictures
    :param: speed: number of time each waveform will be written
    :return: a tuple of L then R out signals
    '''
    out_signal_L = []
    out_signal_R = []
    for i in range(len(A[0])):
        complex_entry = list(A[0][i] + complex(0,1)*A[1][i])
        complex_entry_L = [0]
        complex_entry_R = [0]
        for j in range(len(complex_entry)):
            pan = A[2][i][j]/255
            complex_entry_L.append(pan*complex_entry[j])
            complex_entry_R.append((1-pan)*complex_entry[j])
        partial_complex_out_L = np.fft.ifft(np.array(complex_entry_L))
        partial_complex_out_R = np.fft.ifft(np.array(complex_entry_R))
        out_signal_L.extend(partial_complex_out_L*speed)
        out_signal_R.extend(partial_complex_out_R*speed)
    return (out_signal_L, out_signal_R)


A = np.array([[[0.2, 0.7, 1], [1, 1, 0], [1, 0, 1]], [[0.2, 0.7, 0.3], [1, 1, 0], [1, 0, 1]], [[0.2, 0.5, 1], [1, 1, 0], [1, 0, 1]]])
create_sound_list_RGB(A, 2)

def normalisation(sound):
    '''
    normalises towards 16 bit integers
    :param sound: list
    :return: the nomalised list
    '''

    #find the max in abs
    def abs_max(list):
        max = 0
        for i in list:
            if abs(i)>max:
                max = abs(i)
        return max
    max = abs_max(sound)

    #normalises the list towards a 16 bit integer
    factor = 32767//max
    normalized_sound = []
    for i in sound:
        normalized_sound.append(int(abs(i * factor)))
    return normalized_sound

def normalisation_test():
    L = [0, 3, 2, 1.67, -9, -20, 17, 32]
    #print(normalisation(L))
    NL = normalisation(L)
    if max(NL) > 32767:
        return 'problem with the size -- too big'
    if max(NL) < 32700:
        return 'problem with the size -- too small'
    for x in NL:
        if type(NL) != int:
            return 'not integers'
    return 'OK'

#normalisation_test()


def out_wave_stereo_RGB(filename, data_L, data_R):
    '''
    writes the wave file into created_sounds
    :param filename: string, name that we want for the file
    :param data: list = sound
    '''
    data = []
    for i in range(len(data_L)):
        data.append([data_L[i], data_R[i]])
    data2 = np.asarray(data, dtype = np.int16)
    rate = 44100
    wave.write(filename, rate, data2)

def creation_random_lists():
    A = []
    for i in range(44100):
        A.append(np.random.rand())
    #out_wave_mono('test1', normalisation(A))


