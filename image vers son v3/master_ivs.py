import sys
sys.path.append('D:\02 Cours\02 Cours Centrale\02 - 1A\07 Coding week - applis audio en pyhton_yolo\soundpainting\image vers son v3')

import os

import sound_creation_ivs as sc
import image_mise_en_forme_ivs as imf

def une_image_vers_son_RGB(file, frequency, speed, name):
    '''
    converts an image into sound
    :param file: in path
    :param frequency: base frequency for the conversion (don't go under 50 aproximately, it makes python crash
    :param speed: number of times each waveform will be played (higher number means a slower progression)
    :param name: out path
    :return:
    '''
    A = imf.pic_to_array_RGB(file, frequency)
    SL, SR = sc.create_sound_list_RGB(A, speed)
    NSL = sc.normalisation(SL)
    NSR = sc.normalisation(SR)
    #name_str = str(name)
    sc.out_wave_stereo_RGB(name + '.wav', NSL, NSR)

def soundbank_creating(directory, frequency, speed):
    '''
    converts all the images in a directory to sounds into another directory
    '''
    for f in os.listdir(directory):
        #print ("'%s'" % f)
        #print(f)
        file = ("%s"%directory) + '/' + ("%s"%f)
        print(file)
        out = 'created sounds' + '/' + 'speed=' + ("%s"%speed) + ' freq=' + ("%s"%frequency) + ("%s"%f)
        une_image_vers_son_RGB(file, frequency, speed, out)

#soundbank_creating('images_test', 100, 5)

#une_image_vers_son_RGB('images_test/tableaux-sur-toile-resume-lignes-colorees-sur-fond-noir-freezelight.jpg', 60, 1, 'created sounds/testRGB')
