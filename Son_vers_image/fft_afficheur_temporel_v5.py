import math
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import scipy.io.wavfile as wave
from numpy.fft import fft
from matplotlib.colors import LinearSegmentedColormap
import os

#Path to the directory containing sounds
spath = "c:\\users\\nathan\\onedrive\\Année 2018-2019\\Coding Weeks\\sounds\\"

#Path to the directory for saving images

ipath = "c:\\users\\nathan\\onedrive\\Année 2018-2019\\Coding Weeks\\images_2\\"


#Names of files

name_list = os.listdir(path = spath)

piano = 'piano_la3.wav'
transe = 'transe.wav'
wonky = 'wonky.wav'
complex = "complex_sound.wav"
ville = "ville.wav"
eye = "eye.wav"
bonjour = "bonjour.wav"

#Defining useful constants



# data = data [n//3:2*n//3]
# n = n//3





#Definition d'une fonction qui remplit la colonne i de la matrice
def remplir(i,matrice,dt,donnee,rate,hauteur):
    start = i*dt
    stop = start + dt-1
    spectre =   np.absolute(fft(donnee[start:stop],n=44100))
    n = spectre.size
    freq = np.zeros(n)
    for k in range(n):
        freq[k] = 1.0/n*rate*k
    for k in range(0,hauteur):
        matrice[k,i]=spectre[k]
    return



# fig,axe=plt.subplots(constrained_layout=True)
# axe.set_title('Spectrogramme')
# axe.set_xlabel('temps')
# axe.set_ylabel('fréquence')

#Création colormap

# colors = [(0,0,0),(0,0,1),(0,1,0),(1,0,0)]
# N = 256
# cmperso = LinearSegmentedColormap.from_list('spectro',colors, N)
# #cmperso2 = C





def spectro (nom,save):
    rate,data = wave.read(spath+nom)
    n = len(data)
    div =n//666
    time = n//div
    #Degrading wave date from stero to mono
    
    data_mono = data
    # for i in data:
    #     data_mono.append(i[0])
    
    
    #Initialisation matrice
    hauteur = 9000
    img = np.zeros((hauteur,div))


    #Remplissage de la matrice et chargement
    for i in range(div):
        remplir(i,img,time,data_mono,rate,hauteur)
        if i%(div//10) == 0:
            print(math.floor(i/div*100),"%")
    
    
    #Affichage
    plt.imshow(img, cmap=plt.cm.inferno,interpolation = 'nearest', aspect = 1/30.0,vmax=1/5*img.max(), origin="upper")
    #plt.axis([0,2*div-1,0,10000])
    #plt.yscale('log')
    plt.axis('off')
    #plt.show()
    plt.savefig(save+nom[:-3]+'png',bbox_inches='tight')


def banque_img (li,save):
    for nom in li:
        spectro(nom, save)
    return

    