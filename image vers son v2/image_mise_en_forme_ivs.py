import numpy as np
from PIL import Image


def ouvre(chemin):
    '''
    :param chemin: string
    :return: objet image au sens de PIL
    '''
    return Image.open(chemin) # ouvre le fichier image

def pic_to_array(chemin, frequency, couleur = False):
    """
    :param chemin: string
    :param couleur: bool
    :param frequency: choose the output base frenquency in Hz
    :return: array des colonnes de l'image
    """
    base = ouvre(chemin)
    size = int(22050/frequency)
    # on redimensionne l'image pour la future FFT en convervant le rapport hauteur/largeur
    base = base.resize(( int(size*base.size[0]/base.size[1]) , int(size)))

    # base.size = (largeur, hauteur)
    if couleur:
        base2 = base.convert('RGB')
        # on convertit l'image au format bien
    else:
        base2 = base.convert('L')
    imm = np.asarray(base2)
    # on convertit l'image en array
    return np.transpose(imm)


#print(pic_to_array('C:/Users/Enguerrand Monard/Desktop/Capture1.PNG'))

#L = (255/11)*np.arange(12).reshape((3,4))
#print(L)
#im = Image.fromarray(L)
#im = im.resize(( int(2183*im.size[0]/im.size[1]) , 2183))
#im = im.convert('RGB')
#imm = np.asarray(im)
#print(np.transpose(imm))

# print(base.format, base.size, base.mode)
