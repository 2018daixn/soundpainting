import sys
sys.path.append('D:\02 Cours\02 Cours Centrale\02 - 1A\07 Coding week - applis audio en pyhton_yolo\soundpainting\image vers son v2')
sys.path.append('C:/Users/Enguerrand\ Monard/soundpainting/image vers son v2')

import os

import sound_creation_ivs as sc
import pitchstretch as ps
import image_mise_en_forme_ivs as imf

def une_image_vers_son(file, frequency, speed, downpitch, name):
    '''
    converts an image into sound
    :param file: in path
    :param frequency: base frequency for the conversion (don't go under 50 aproximately, it makes python crash
    :param speed: number of times each waveform will be played (higher number means a slower progression)
    :param downpitch: downs the pitch in Hz
    :param name: out path
    :return:
    '''
    A = imf.pic_to_array(file, frequency, False)
    S = sc.create_sound_list(A, speed)
    NS = sc.normalisation(S)
    PS = ps.pitchstretch(NS, 40, 44100, -downpitch)
    name_str = str(name)
    sc.out_wave_mono(name_str + '.wav', PS)

def soundbank_creating(directory, frequency, speed, downpitch):
    '''
    converts all the images in a directory to sounds into another directory
    '''
    for f in os.listdir(directory):
        #print ("'%s'" % f)
        #print(f)
        file = ("%s"%directory) + '/' + ("%s"%f)
        print(file)
        out = 'created sounds' + '/' + 'speed=' + ("%s"%speed) + ' freq=' + ("%s"%frequency) + ("%s"%f)
        une_image_vers_son(file, frequency, speed, downpitch, out)

#soundbank_creating('images_test', 100, 1, 2000)
#une_image_vers_son('image_test_paint/text.jpg', 100, 4, 1000, 'created sounds/code_text')
#une_image_vers_son('image_test_paint/image test paint 2j.jpg', 60, 1, 2000, 'created sounds/test paint')
