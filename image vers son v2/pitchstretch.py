import scipy.io.wavfile as wave
import numpy as np
import matplotlib.pyplot as plt

__name__ = 'pitchstretch'

def test_with_external_audio():      #precise test

    #test files
    file = 'sounds\sine_la_440.wav'
    freqshift = -100
    framerate = 20

    samplerate, data_source = wave.read(file)  #reads fe and the wave

    print(samplerate)

    def passage_en_mono(data):
        '''
        :param data: issue d'une extraction .wav
        :return: liste en mono si elle était en stéréo
        '''
        if len(data[0]) == 2:      #si le fichier est en stéréo, on passe en mono
            data_mono = []
            for i in range(len(data)):
                data_mono.append((data[i][0]+data[i][1])//2)    #on remplace la liste [gauche, droite] par moyenne (passage de stereo à mono)
            return data_mono
        else:
            return data_source

    #on lit le fichier pour en faire une liste mono

    data_mono = passage_en_mono(data_source)
    out = pitchstretch(data_mono, framerate, samplerate, freqshift)
    wave.write('la_down_test.wav', samplerate, np.array(out))

    #displaying
    X = range(len(data_mono))
    plt.plot(X, data_mono)
    X2 = range(len(out))
    plt.plot(X2, out)
    plt.show()



def pitchstretch(data, framerate, samplerate, freqshift):
    '''

    :param data: data to stretch
    :param framerate: rate : test 20
    :param samplerate: 44100 Hz basically
    :param freqshift: shifting in Hz
    :return:
    '''
    #initialisation des valeurs pour le PS
    shift = freqshift//framerate
    out = []
    frame_size = samplerate//framerate
    nb_frames = len(data)//frame_size

    #lancement du PS frame à frame
    for i in range(nb_frames):
        frame_current = data[i*frame_size:(i+1)*frame_size]
        #Extract the frequencies using the Fast Fourier Transform built into numpy
        file_fourrier = np.fft.rfft(frame_current)
        #Roll the array to increase the pitch.
        array_rolled = np.roll(file_fourrier, shift)
        if shift >= 0:
            #The highest frequencies roll over to the lowest ones. That's not what we want, so zero them.
            array_rolled[0:shift] = 0
        else:
            #The lowest frequencies roll over to the highest ones. That's not what we want, so zero them.
            array_rolled[shift:-1] = 0
        #Now use the inverse Fourier transform to convert the signal back into amplitude.
        signal_amp_ps = np.fft.irfft(array_rolled)
        out.extend(signal_amp_ps)

    return (out)



