import sys
sys.path.append('C:/Users/Enguerrand\ Monard/soundpainting/image vers son')

# test de image_mise_en_forme_ivs.py
import image_mise_en_forme_ivs
import unittest

class TestConvertit(unittest.TestCase):
    """
    Test the convert function from the mymath library
    """

    def test_ouvre(self):
        """
        Test that the addition of two integers returns the correct total
        """
        result = image_mise_en_forme_ivs.add(1, 2)
        self.assertEqual(result, 3)

    def test_pic_to_array(self):
        """
        Test that the conversion of a picture returns the correct result
        """
        result = image_mise_en_forme_ivs.add(10.5, 2)
        self.assertEqual(result, 12.5)




if __name__ == '__main__':
    unittest.main()
