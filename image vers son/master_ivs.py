__name__ = 'master_ivs'

import sys
sys.path.append('D:\02 Cours\02 Cours Centrale\02 - 1A\07 Coding week - applis audio en pyhton_yolo\soundpainting\image vers son')

import os

import sound_creation_ivs as sc
import image_mise_en_forme_ivs as imf

def une_image_vers_son(file, name):
    A = imf.pic_to_array(file)
    S = sc.create_sound_list(A)
    NS = sc.normalisation(S)
    name_str = str(name)
    sc.out_wave_mono(name_str + '.wav', NS)

def soundbank_creating(directory):
    for f in os.listdir(directory):
        #print ("'%s'" % f)
        #print(f)
        file = ("%s"%directory) + '/' + ("%s"%f)
        print(file)
        out = 'created sounds' + '/' + ("%s"%f)
        une_image_vers_son(file, out)

#soundbank_creating('image_test_paint')
