import numpy as np
import matplotlib.pyplot as plt
import scipy.io.wavfile as wave

#mono input, coding a wav in stereo
#fake stereoing


def create_sound_list(A):
    '''
    creates a list corresponding to the time view of the matrix A with columns as frequencies and lines as time
    :param A: list of lists
    :return: a list corresp to the time domain
    '''
    out_signal = []
    for a in A:
        partial_out = np.fft.irfft([0] + a)
        out_signal.append(partial_out)
    return out_signal



def normalisation_stereo(sound):
    '''
    normalises towards 16 bit integers
    :param sound: list
    :return: the nomalised list
    '''

    #find the max in abs
    def abs_max(list):
        max = 0
        for i in list:
            if abs(i)>max:
                max = abs(i)
        return max
    max = abs_max(sound)

    #normalises the list towards a 16 bit integer
    factor = 32767//max
    normalized_sound = []
    for i in sound:
        v = int(i * factor)
        normalized_sound.append([v, v])
    return normalized_sound



def out_wave(filename, data):
    '''
    writes the wave file into created_sounds
    :param filename: string, name that we want for the file
    :param data: list = sound
    '''
    data2 = np.asarray(data, dtype = np.int16)
    rate = 44100
    wave.write(filename, rate, data2)

