__name__ = 'sound_creation_ivs'

import numpy as np
import matplotlib.pyplot as plt
import scipy.io.wavfile as wave


def create_sound_list(A, print=0):
    '''
    creates a list corresponding to the time view of the matrix A with columns as frequencies and lines as time
    :param A: list of lists
    :param print: set to 1 to print the returned out_signal
    :return: a list corresp to the time domain
    '''
    out_signal = []
    for a in A:
        partial_out = np.fft.irfft([0] + list(a))   #[0] pour commencer à une fréquence de 1 unité
        out_signal.extend(partial_out)

    #print(out_signal)
    if print:
        n = len(out_signal)
        X = np.arange(n)
        plt.plot(X, out_signal)
        plt.show()

    return out_signal



#create_sound_list([spectre], print=1)

#spectre[880] = 0.5
#partial_out = np.fft.irfft(spectre)   #[0]*255 pour commencer à un la 440 avec l'échantillonage prévu à 44100Hz, 1 valeur toute es 10Hz
#on resize à la taille de la waveform, sine à 440 (la), soit on garde 10 dots
#partial_out_resized = partial_out[:20]     #il faut 20 POINTS !!!!
#n = len(partial_out_resized)
#X = np.arange(n)
#plt.plot(X, partial_out_resized)
#plt.show()




#tests
#A = [[1, 2, 4, 5], [2, 3, 5, 9], [12, 17, 2, 1]]
#B = [[np.random.rand() for i in range(500)] for i in range(500)]
#print(B)
#create_sound_list(B)

# --> taille de la sortie en longueur et hauteur ?
# normaliser le level avec un format wav d output
# normaliser la freq échantillonage selon ce qui est voulu 44100 Hz


def normalisation(sound):
    '''
    normalises towards 16 bit integers
    :param sound: list
    :return: the nomalised list
    '''

    #find the max in abs
    def abs_max(list):
        max = 0
        for i in list:
            if abs(i)>max:
                max = abs(i)
        return max
    max = abs_max(sound)

    #normalises the list towards a 16 bit integer
    factor = 32767//max
    normalized_sound = []
    for i in sound:
        normalized_sound.append(int(i * factor))

    return normalized_sound

def normalisation_test():
    L = [0, 3, 2, 1.67, -9, -20, 17, 32]
    #print(normalisation(L))
    NL = normalisation(L)
    if max(NL) > 32767:
        return 'problem with the size -- too big'
    if max(NL) < 32700:
        return 'problem with the size -- too small'
    for x in NL:
        if type(NL) != int:
            return 'not integers'
    return 'OK'

#normalisation_test()


def out_wave_mono(filename, data):
    '''
    writes the wave file into created_sounds
    :param filename: string, name that we want for the file
    :param data: list = sound
    '''
    data2 = np.asarray(data, dtype = np.int16)
    rate = 44100
    wave.write(filename, rate, data2)

def creation_random_lists():
    A = []
    for i in range(44100):
        A.append(np.random.rand())
    #out_wave_mono('test1', normalisation(A))


