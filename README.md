# SoundPainting

Literally painting with sound.

Ce projet a pour but de "convertir" des images en son et réciproquement.

Plus précisément, il s'agira d'exploiter les propriétés d'un son à l'aide de la transformée de Fourier et de leur faire correspondre des propriétés propres aux images.

De même, une image peut être convertie en son à l'aide de la transformée de Fourier inverse.

On va de plus coder une interface entre l'utilisateur et la machine pour :

 + convertir un son en image ou l'inverse avec la méthode ci-dessus en allant chercher le fichier dans l'explorateur de fichiers
 + à partir d'une base de données locale de sons et d'images, créer un jeu dans lequel l'utilisateur entend un son préexistant et doit choisir parmi 3 images celle qui correspond (les 3 images sont le résultat d'une conversion d'un son en image par notre algorithme)


Liste des paquets à installer pour que le projet fonctionne :

+ Pillow
+ tkinter
+ numpy
+ sys
+ math
+ matplotlib
+ scipy
+ os

Version de Python conseillée :

3.6